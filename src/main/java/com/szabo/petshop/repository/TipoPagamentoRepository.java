package com.szabo.petshop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.szabo.petshop.model.TipoPagamento;

/***
 * Essa Interface é responsavel por criar o repositorio do  Tipo de Pagamento, 
 * 
 * @author Matheus Glauber
 *
 */
@Repository
public interface TipoPagamentoRepository extends JpaRepository<TipoPagamento, Long> {

}
