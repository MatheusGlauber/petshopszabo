package com.szabo.petshop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.szabo.petshop.model.Tracking;

/***
 * Essa Interface é responsavel por criar o repositorio de Tracking, 
 * 
 * @author Matheus Glauber
 *
 */
@Repository
public interface TrackingRepository extends JpaRepository<Tracking, Long>{

}
