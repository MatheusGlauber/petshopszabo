package com.szabo.petshop.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.szabo.petshop.dto.CadastrarTrackingDTO;
import com.szabo.petshop.model.Pedido;
import com.szabo.petshop.model.Tracking;
import com.szabo.petshop.repository.TrackingRepository;
import com.szabo.petshop.service.mapper.TrackingMapper;

/***
 * Responsável por implementar o service do Tracking
 * @author Matheus Glauber
 *
 */
@Service
public class TrackingService {

	@Autowired
	private TrackingRepository trackingRepository;

	@Autowired
	private PedidoService pedidoService;

	@Autowired
	private StatusService statusService;

	public Tracking inserirTracking(CadastrarTrackingDTO trackingDTO) {

		statusService.inserirStatus(trackingDTO.getStatus());
		Pedido pedido = pedidoService.getById(trackingDTO.getIdPedido());
		Tracking tracking = TrackingMapper.mapper(trackingDTO);
		if (pedido != null) {
			tracking.setStatus(statusService.getById(trackingDTO.getStatus().getId()));
			tracking.setPedido(pedido);
			tracking.setDescricao("Pedido feito com sucesso. Aguarde chegar");
			tracking.getStatus().setDescricao("Pedido a caminho");
		}
		return trackingRepository.save(tracking);
	}

	public Tracking update(Tracking tracking, Long id) {
		Tracking t = getById(id);
		t.setDataStatus(tracking.getDataStatus());
		t.setDescricao(tracking.getDescricao());
		t.setGeo_long(tracking.getGeo_long());
		t.setGeoLat(tracking.getGeoLat());
		t.setPedido(tracking.getPedido());
		t.setStatus(tracking.getStatus());
		return trackingRepository.save(t);

	}

	public void delete(Long id) {
		trackingRepository.deleteById(id);
	}

	public Tracking getById(Long id) {
		Tracking tracking = trackingRepository.findById(id).get();
		return tracking;
	}

	public List<Tracking> findAll() {
		List<Tracking> trackings = trackingRepository.findAll();
		return trackings;
	}

}
