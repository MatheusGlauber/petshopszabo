package com.szabo.petshop.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.szabo.petshop.model.TipoPagamento;
import com.szabo.petshop.repository.TipoPagamentoRepository;

/***
 * Responsável por implementar o service do Tipo de Pagamento
 * @author Matheus Glauber
 *
 */
@Service
public class TipoPagamentoService {
	
	@Autowired
	private TipoPagamentoRepository tipoRepository;
	
	/***
	 * Metodo que pega um pagamento pelo id;
	 * @param id
	 * @return
	 */
	public TipoPagamento getById(Long id) {
		return tipoRepository.findById(id).get();
	}
	
	/***
	 * Metodo que insere um tipo de pagamento
	 * @param tipoPagamento
	 */
	public void inserirTipo(TipoPagamento tipoPagamento) {
		tipoRepository.save(tipoPagamento);
	}

}
