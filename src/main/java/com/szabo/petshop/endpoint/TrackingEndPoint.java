package com.szabo.petshop.endpoint;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.szabo.petshop.dto.CadastrarTrackingDTO;
import com.szabo.petshop.model.Tracking;
import com.szabo.petshop.service.TrackingService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/***
 * End point do rastreamento
 * 
 * @author Matheus Glauber
 *
 */
@Api(value="Controller-Tracking")
@RestController
@RequestMapping("/tracking")
public class TrackingEndPoint {
	
	@Autowired
	private TrackingService trackingService;
	
	@ApiOperation("value=Retorna um pedido pelo id")
	@GetMapping("{id}")
	public ResponseEntity<Tracking> getId(@PathVariable Long id){
		return new ResponseEntity<Tracking>(trackingService.getById(id), HttpStatus.OK);
		
	}
	
	@ApiOperation(value="Insere tracking")
	@PostMapping
	public ResponseEntity<Tracking> save(@RequestBody CadastrarTrackingDTO trackingDTO){
		Tracking track = trackingService.inserirTracking(trackingDTO);
		return new ResponseEntity<Tracking>(track, HttpStatus.CREATED);
	}
	
	@ApiOperation(value="Lista todos os trackings")
	@GetMapping
	public ResponseEntity<List<Tracking>> findAllTracking(){
		return new ResponseEntity<List<Tracking>>(trackingService.findAll(), HttpStatus.OK);
	}
	
	@ApiOperation(value="Deleta o tracking pelo id")
	@DeleteMapping("{id}")
	public ResponseEntity<Tracking> deleteTracking(@PathVariable Long id){
		trackingService.delete(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@ApiOperation(value="Atualiza o tracking pelo id")
	@PutMapping("{id}")
	public ResponseEntity<Tracking> updateCategoria(@PathVariable Long id, @RequestBody Tracking tracking){
		trackingService.update(tracking,id);
		return new ResponseEntity<>(HttpStatus.OK);
	}
}
