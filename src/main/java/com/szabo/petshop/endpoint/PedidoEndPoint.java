package com.szabo.petshop.endpoint;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.szabo.petshop.model.Pedido;
import com.szabo.petshop.service.PedidoService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/***
 * End Point do pedido
 * @author Matheus Glauber
 *
 */
@Api(value="Controller-Pedido")
@RestController
@RequestMapping("/pedido")
public class PedidoEndPoint {
	
	@Autowired
	private PedidoService pedidoService;
	
	@ApiOperation("value=Retorna um pedido pelo id")
	@GetMapping("{id}")
	public ResponseEntity<Pedido> getId(@PathVariable Long id){
		return new ResponseEntity<Pedido>(pedidoService.getById(id), HttpStatus.OK);
		
	}
	
	@ApiOperation(value="Insere pedido")
	@PostMapping
	public ResponseEntity<Pedido> save(@RequestBody Pedido pedido){
		return new ResponseEntity<Pedido>(pedidoService.inserirPedido(pedido),HttpStatus.CREATED);
	}

		
	@ApiOperation(value="Lista todos os pedidos")
	@GetMapping
	public ResponseEntity<List<Pedido>> findAllTracking(){
		return new ResponseEntity<List<Pedido>>(pedidoService.findAll(), HttpStatus.OK);
	}
	
	@ApiOperation(value="Deleta o pedido pelo id")
	@DeleteMapping("{id}")
	public ResponseEntity<Pedido> deleteTracking(@PathVariable Long id){
		pedidoService.delete(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@ApiOperation(value="Atualiza o pedido pelo id")
	@PutMapping("{id}")
	public ResponseEntity<Pedido> updateCategoria(@PathVariable Long id, @RequestBody Pedido pedido){
		pedidoService.update(pedido,id);
		return new ResponseEntity<>(HttpStatus.OK);
	}
}
