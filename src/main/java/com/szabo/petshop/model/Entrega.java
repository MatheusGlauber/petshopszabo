package com.szabo.petshop.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.springframework.format.annotation.DateTimeFormat;

/***
 * Entidade Responsável por modelar a entrega
 * 
 * @author Matheus Glauber
 *
 */
@Entity
public class Entrega {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "previsto_para")
	private LocalDate previstoEntrega;

	@ManyToOne
	@JoinColumn(name = "cod_pedido")
	private Pedido pedido;

	@DateTimeFormat
	private LocalDate dataStatus;

	public String getCod_status() {
		return cod_status;
	}

	public void setCod_status(String cod_status) {
		this.cod_status = cod_status;
	}

	private String cod_status;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDate getPrevistoEntrega() {
		return previstoEntrega;
	}

	public void setPrevistoEntrega(LocalDate previstoEntrega) {
		this.previstoEntrega = previstoEntrega;
	}

	public LocalDate getDataStatus() {
		return dataStatus;
	}

	public void setDataStatus(LocalDate dataStatus) {
		this.dataStatus = dataStatus;
	}

	public Pedido getPedido() {
		return pedido;
	}

	public void setPedido(Pedido pedido) {
		this.pedido = pedido;
	}

}
