package com.szabo.petshop.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.szabo.petshop.model.enums.StatusPagamentoEnum;
import com.szabo.petshop.model.enums.TipoPagamentoEnum;

/***
 * Entidade Responsável por modelar o Tipo de Pagamento
 * 
 * @author Matheus Glauber
 *
 */
@Entity
@Table(name = "tipo_pgto")
public class TipoPagamento {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Enumerated(EnumType.STRING)
	@Column(name = "nome_tipo_pagto")
	private TipoPagamentoEnum nomeTipoPgto;

	@Enumerated(EnumType.STRING)
	private StatusPagamentoEnum statusPgto;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TipoPagamentoEnum getNomeTipoPgto() {
		return nomeTipoPgto;
	}

	public void setNomeTipoPgto(TipoPagamentoEnum nomeTipoPgto) {
		this.nomeTipoPgto = nomeTipoPgto;
	}

	public StatusPagamentoEnum getStatusPgto() {
		return statusPgto;
	}

	public void setStatusPgto(StatusPagamentoEnum statusPgto) {
		this.statusPgto = statusPgto;
	}

}
