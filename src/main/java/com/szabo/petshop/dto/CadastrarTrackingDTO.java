package com.szabo.petshop.dto;

import java.io.Serializable;
import java.time.LocalDate;

import com.szabo.petshop.model.Status;

/***
 * DTO para cadastrar os rastreamentos
 * 
 * @author Matheus Glauber
 *
 */
public class CadastrarTrackingDTO implements Serializable {

	private static final long serialVersionUID = -2541990748260798501L;

	private Long idPedido;

	private String descricao;

	private LocalDate dataStatus;

	private String geoLat;

	private String geo_long;

	private Status status;

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Long getIdPedido() {
		return idPedido;
	}

	public void setIdPedido(Long idPedido) {
		this.idPedido = idPedido;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public LocalDate getDataStatus() {
		return dataStatus;
	}

	public void setDataStatus(LocalDate dataStatus) {
		this.dataStatus = dataStatus;
	}

	public String getGeoLat() {
		return geoLat;
	}

	public void setGeoLat(String geoLat) {
		this.geoLat = geoLat;
	}

	public String getGeo_long() {
		return geo_long;
	}

	public void setGeo_long(String geo_long) {
		this.geo_long = geo_long;
	}

}
